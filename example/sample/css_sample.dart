
import 'dart:html';
import 'package:angular/angular.dart';

/// 20210120
@Component(
    selector: 'sample',
    template: '<h1 #el>sample</h1>',
)
class CssSample implements OnInit {

    @ViewChild('el')
    HtmlElement? el;

    @override
    void ngOnInit() {

        if (el != null) {
            
            const textColorA = 'red';
            const textColorB = 'blue';
            const backgroundColor = 'black';
            const overflow = 'scroll';
            
            el?.style.color = textColorA;
            el?.style.backgroundColor = backgroundColor;
            el?.style.overflow = overflow;

            /// MediaQuary ブラウザのサイズが変化したら発火する
            const minWidth = '800px';
            const maxWidth = '1600px';
            final mediaQueryListMin = window.matchMedia('screen and ( min-width:${minWidth} )');
            final mediaQueryListMax = window.matchMedia('screen and ( max-width:${maxWidth} )');
            
            mediaQueryListMin.addListener((event) {
                if(mediaQueryListMin.matches) {
                    el?.style.color = textColorA;
                    print('min fire');
                }
            });

            mediaQueryListMax.addListener((event) {
                if(mediaQueryListMax.matches) {
                    el?.style.color = textColorB;
                    print('max fire');
                }
            });

        };

    }

}
