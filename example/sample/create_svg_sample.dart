
import 'dart:html';
import 'dart:svg';
import 'package:angular/angular.dart';

/// 20210120
@Component(
    selector: 'sample',
    template: '<h1 #el>sample</h1>',
)
class CreateSvgSample implements OnInit {

    @ViewChild('el')
    HtmlElement? el;

    AnimateElement createAnimationElement() {

        final animate = AnimateElement();
        
        /// これでも生成できる。
        // final animate = SvgElement.tag('animate');
        
        /// AnimationElement() だと、生成できない。
        /// AnimationElement の super class である、AnimateElement を間違えずに使う（経験談）。
        // final animate = AnimationElement();
        
        /// document.createElementNS() で、生成できるが、dart:html に依存しなければならない。
        /// dart:svg, dart:html, はともに、web（browser）の環境に依存するライブラリであるが、
        /// 名前の競合がよく起きるので、なるべく避けた方が良い。
        /// あと、書き方が少し冗長。
        // final animate = document.createElementNS('http://www.w3.org/2000/svg', 'animate');

        /// 下記のように、setAttribute() を用いて、各種設定などを行い利用する。
        animate.setAttribute('attributeName', 'd');
        animate.setAttribute('values', '...省略...');
        animate.setAttribute('dur', '2s');
        animate.setAttribute('repeatCount', '...省略...');

        return animate /* as AnimateElement */;

    }

    /// EllipseElement, PathElement, ImageElement, AnimateElement, SvgSvgElement なども要領はこの RectElement と同じ。
    RectElement createRectElement() {
        
        /// AnimationElement と同様に、SvgElement.tag()、document.createElementNS()、でも生成可能だが、下記が一番良い方法。
        final rectElement = RectElement();

        /// AnimationElement と同様に、setAttribute() を用いて、各種設定などを行い利用する。
        rectElement.setAttribute('fill', '...省略...');
        rectElement.setAttribute('x', '...省略...');
        rectElement.setAttribute('y', '...省略...');
        rectElement.setAttribute('width', '...省略...');
        rectElement.setAttribute('height', '...省略...');

        return rectElement;

    }

    @override
    void ngOnInit() {

        if (el != null) {

            /// TODO まず SvgSvgElement を生成する。

        };

    }

}
