
import 'package:angular/angular.dart';

import 'template_tips_sample.template.dart' as temp; // これ

/// 
/// 20210121
/// 
/// 初期設定の例はここから引用
/// https://github.com/dart-lang/stagehand/blob/master/templates/web-angular/web/main.dart
/// 
@Component(
    selector: 'sample',
    template: '<h1>sample</h1>',
)
class TemplateTipsSample {

    static final template = temp.TemplateTipsSampleNgFactory; // あらかじめ 自 class の static 変数に格納しておく

}
