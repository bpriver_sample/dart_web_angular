
import 'package:angular/angular.dart';
import 'package:pub_base_model/material.dart' as base;
import 'package:sample_dart_web_angular/dart_web_angular.dart';
import 'package:sample_dart_web_angular/src/ayu/ayu_barrel.dart';
import 'package:sample_dart_web_angular/src/ayu/directive/back_screen/back_screen_directive.dart';
import 'package:sample_dart_web_angular/src/container/presentation_change/presentation_change_directive.dart';
import 'package:sample_dart_web_angular/src/presentation/final_input_status/final_input_status_directive.dart';

import '../sample/sample_barrel.dart';
import 'model.dart';
import 'root_ac.template.dart' as temp;

@Component(
    selector: 'root',
    styles: [
    ],
    templateUrl: 'root_ac.html',
    directives: [
        NgFor,
        TemplateTipsSample,

        FinalInputStatusDirective,
        PresentationChangeDirective,

        GameDirective,
        WindowDirective,
        WindowDirectiveAC,
        ViewportDirective,
        BackScreenDirective,
    ],
    providers: [
        Operate,
        Store,
    ],
    exports: [
        Model,
    ]
)
class RootACDirective
    extends
        base.Material
    implements
        OnInit
{

    static final template = temp.RootACDirectiveNgFactory;

    RootACDirective(this.store, this.operate) {
                store.windowConfigurationSet = MutableWindowConfigurationSet([
            MutableWindowConfiguration(WindowConfiguration(
                WindowStatus(
                    WindowStatusIdentifier(),
                    WindowStatusWidth(Model.px),
                    WindowStatusHeight(Model.px),
                    WindowStatusXAxis(0),
                    WindowStatusYAxis(Model.px*(18-1)),
                    WindowStatusPaddingTop(0),
                    WindowStatusPaddingBottom(0),
                    WindowStatusPaddingLeft(0),
                    WindowStatusPaddingRight(0),
                ),
                SpriteBackgroundStatusList([
                    SpriteBackgroundStatus(
                        SpriteBackgroundStatusSrc('sample_sprite_pomeranians.png'),
                        SpriteBackgroundStatusSpriteSheetColumns(12),
                        SpriteBackgroundStatusSpriteSheetRows(8),
                        SpriteBackgroundStatusRangeBeginX(1),
                        SpriteBackgroundStatusRangeBeginY(1),
                        SpriteBackgroundStatusRangeEndX(3),
                        SpriteBackgroundStatusRangeEndY(1),
                        SpriteBackgroundStatusSituation.initial(),
                        SpriteBackgroundStatusDuration(1000),
                        SpriteBackgroundStatusTurnList([
                            SpriteBackgroundStatusTurn(1),
                        ]),
                    ),
                    SpriteBackgroundStatus(
                        SpriteBackgroundStatusSrc('sample_sprite_pomeranians.png'),
                        SpriteBackgroundStatusSpriteSheetColumns(12),
                        SpriteBackgroundStatusSpriteSheetRows(8),
                        SpriteBackgroundStatusRangeBeginX(1),
                        SpriteBackgroundStatusRangeBeginY(1),
                        SpriteBackgroundStatusRangeEndX(3),
                        SpriteBackgroundStatusRangeEndY(1),
                        SpriteBackgroundStatusSituation(Situation.walkForward),
                        SpriteBackgroundStatusDuration(1000),
                        SpriteBackgroundStatusTurnList([
                            SpriteBackgroundStatusTurn(1),
                            SpriteBackgroundStatusTurn(0),
                            SpriteBackgroundStatusTurn(1),
                            SpriteBackgroundStatusTurn(2),
                        ]),
                    ),
                    SpriteBackgroundStatus(
                        SpriteBackgroundStatusSrc('sample_sprite_pomeranians.png'),
                        SpriteBackgroundStatusSpriteSheetColumns(12),
                        SpriteBackgroundStatusSpriteSheetRows(8),
                        SpriteBackgroundStatusRangeBeginX(1),
                        SpriteBackgroundStatusRangeBeginY(2),
                        SpriteBackgroundStatusRangeEndX(3),
                        SpriteBackgroundStatusRangeEndY(2),
                        SpriteBackgroundStatusSituation(Situation.walkLeft),
                        SpriteBackgroundStatusDuration(200),
                        SpriteBackgroundStatusTurnList([
                            SpriteBackgroundStatusTurn(1),
                            SpriteBackgroundStatusTurn(0),
                            SpriteBackgroundStatusTurn(1),
                            SpriteBackgroundStatusTurn(2),
                        ]),
                    ),
                    SpriteBackgroundStatus(
                        SpriteBackgroundStatusSrc('sample_sprite_pomeranians.png'),
                        SpriteBackgroundStatusSpriteSheetColumns(12),
                        SpriteBackgroundStatusSpriteSheetRows(8),
                        SpriteBackgroundStatusRangeBeginX(1),
                        SpriteBackgroundStatusRangeBeginY(3),
                        SpriteBackgroundStatusRangeEndX(3),
                        SpriteBackgroundStatusRangeEndY(3),
                        SpriteBackgroundStatusSituation(Situation.walkRight),
                        SpriteBackgroundStatusDuration(100),
                        SpriteBackgroundStatusTurnList([
                            SpriteBackgroundStatusTurn(1),
                            SpriteBackgroundStatusTurn(0),
                            SpriteBackgroundStatusTurn(1),
                            SpriteBackgroundStatusTurn(2),
                        ]),
                    ),
                    SpriteBackgroundStatus(
                        SpriteBackgroundStatusSrc('sample_sprite_pomeranians.png'),
                        SpriteBackgroundStatusSpriteSheetColumns(12),
                        SpriteBackgroundStatusSpriteSheetRows(8),
                        SpriteBackgroundStatusRangeBeginX(1),
                        SpriteBackgroundStatusRangeBeginY(4),
                        SpriteBackgroundStatusRangeEndX(3),
                        SpriteBackgroundStatusRangeEndY(4),
                        SpriteBackgroundStatusSituation(Situation.walkBackward),
                        SpriteBackgroundStatusDuration(1000),
                        SpriteBackgroundStatusTurnList([
                            SpriteBackgroundStatusTurn(1),
                            SpriteBackgroundStatusTurn(0),
                            SpriteBackgroundStatusTurn(1),
                            SpriteBackgroundStatusTurn(2),
                        ]),
                    ),
                ]),
                null,
                null,
            )),
        ]);
        store.actorSet = MutableActorSet([
            MutableActor.broadcast(Actor(
                ActorIdentifier(),
                ActorPositionX(0),
                ActorPositionY(0),
                ActorPositionZ(0),
                ActorWidth(0),
                ActorHeight(0),
                ActorDepth(0)
            )),
        ]);
    }
    final Store store;
    final Operate operate;

    late WindowConfiguration windowConf;
    late MutableActor actor;

    var flag = true;
    void auto(MutableWindowConfiguration window, int addition, int max, int mini) {
        final status = window.value.windowStatus;
        if(flag) {
            window.next(
                window.value.changeWindowStatus(status.changeX(WindowStatusXAxis(status.x.value + addition)))
            );
            if(window.value.windowStatus.x.value >= max) flag = false;
        } else {
            window.next(
                window.value.changeWindowStatus(status.changeX(WindowStatusXAxis(status.x.value -addition)))
            );
            if(window.value.windowStatus.x.value <= mini) flag = true;
        }
    }

    @override
    void ngOnInit() {
        printd('root_ac');
        windowConf = store.windowConfigurationSet.first.value;
        final id = store.createActorWithWindow(windowConf.windowStatus.identifier, broadcast: true);
        actor = store.actorSet.getById(id);

        final count = 100;
        for (var i = 0; i < count; i++) {
            final base = windowConf.windowStatus;
            store.createWindowConfiguration(windowConf.changeWindowStatus(WindowStatus(
                WindowStatusIdentifier(),
                base.width,
                base.height,
                base.x,
                base.y - WindowStatusYAxis(i * 10),
                base.paddingTop,
                base.paddingBottom,
                base.paddingLeft,
                base.paddingRight
            )));
        }
        var index = 1;
        printd('store.windowConfigurationSet.length',store.windowConfigurationSet.length);

        final scale = 64;
        var right = 'right';
        var left = 'left';
        var up = 'up';
        var down = 'down';
        var direction = down; // 方向
        var nextPoint = 0;
        var onTheWay = false; // 向かってる途中
        final addition = 5;
        final max = 500;
        final mini = 0;
        operate.onDraw(() {
            var counter = 1;
            store.windowConfigurationSet.forEach((element){
                if(element.identifier == windowConf.windowStatus.identifier) return;
                auto(element, counter, max, mini);
                counter++;
                if(counter >= 15) counter = 1;
            });
            move();
        });
    }

    void move() {
        final addition = 4;
        if(operate.arrowRight) {
            var window = windowConf.windowStatus;
            windowConf = windowConf.changeWindowStatus(window.changeX(WindowStatusXAxis(window.x.value + addition)));
            windowConf = windowConf.changeWindowStatus(windowConf.windowStatus.changeSpriteSituation(SpriteBackgroundStatusSituation(Situation.walkRight)));
        } else if(operate.arrowLeft) {
            var window = windowConf.windowStatus;
            windowConf = windowConf.changeWindowStatus(window.changeX(WindowStatusXAxis(window.x.value - addition)));
            windowConf = windowConf.changeWindowStatus(windowConf.windowStatus.changeSpriteSituation(SpriteBackgroundStatusSituation(Situation.walkLeft)));
        } else if(operate.arrowUp) {
            var window = windowConf.windowStatus;
            windowConf = windowConf.changeWindowStatus(window.changeY(WindowStatusYAxis(window.y.value + addition)));
            windowConf = windowConf.changeWindowStatus(windowConf.windowStatus.changeSpriteSituation(SpriteBackgroundStatusSituation(Situation.walkBackward)));
        } else if(operate.arrowDown) {
            var window = windowConf.windowStatus;
            windowConf = windowConf.changeWindowStatus(window.changeY(WindowStatusYAxis(window.y.value - addition)));
            windowConf = windowConf.changeWindowStatus(windowConf.windowStatus.changeSpriteSituation(SpriteBackgroundStatusSituation(Situation.walkForward)));
        }
    }

}