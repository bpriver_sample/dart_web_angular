
import 'package:angular/angular.dart';
import 'package:pub_base_model/material.dart' as base;
import 'package:sample_dart_web_angular/dart_web_angular.dart';
import 'package:sample_dart_web_angular/src/ayu/ayu_barrel.dart';
import 'package:sample_dart_web_angular/src/ayu/directive/back_screen/back_screen_directive.dart';
import 'package:sample_dart_web_angular/src/container/presentation_change/presentation_change_directive.dart';
import 'package:sample_dart_web_angular/src/presentation/final_input_status/final_input_status_directive.dart';

import '../sample/sample_barrel.dart';
import 'model.dart';
import 'root.template.dart' as temp;

@Component(
    selector: 'root',
    styles: [
        // 'section { position: relative; }',
        // 'section { font-family: "Noto Sans JP","meiryo","メイリオ", sans-serif; }',
        // 'section { overflow: scroll }',
    ],
    // template: '<sample></sample>',
    templateUrl: 'root.html',
    directives: [
        NgFor,
        // ResizeSample,
        // CssSample,
        TemplateTipsSample,

        FinalInputStatusDirective,
        PresentationChangeDirective,

        GameDirective,
        WindowDirective,
        WindowDirectiveAC,
        ViewportDirective,
        BackScreenDirective,
    ],
    providers: [
        Operate,
        Store,
    ],
    exports: [
        Model,
    ]
)
class RootDirective
    extends
        base.Material
    implements
        OnInit
{

    static final template = temp.RootDirectiveNgFactory;

    RootDirective(this.store, this.operate) {
                store.windowConfigurationSet = MutableWindowConfigurationSet([
            MutableWindowConfiguration(WindowConfiguration(
                WindowStatus(
                    WindowStatusIdentifier(),
                    WindowStatusWidth(Model.px),
                    WindowStatusHeight(Model.px),
                    WindowStatusXAxis(0),
                    WindowStatusYAxis(Model.px*(18-1)),
                    WindowStatusPaddingTop(0),
                    WindowStatusPaddingBottom(0),
                    WindowStatusPaddingLeft(0),
                    WindowStatusPaddingRight(0),
                ),
                SpriteBackgroundStatusList([
                    SpriteBackgroundStatus(
                        SpriteBackgroundStatusSrc('sample_sprite_pomeranians.png'),
                        SpriteBackgroundStatusSpriteSheetColumns(12),
                        SpriteBackgroundStatusSpriteSheetRows(8),
                        SpriteBackgroundStatusRangeBeginX(1),
                        SpriteBackgroundStatusRangeBeginY(1),
                        SpriteBackgroundStatusRangeEndX(3),
                        SpriteBackgroundStatusRangeEndY(1),
                        SpriteBackgroundStatusSituation.initial(),
                        SpriteBackgroundStatusDuration(1000),
                        SpriteBackgroundStatusTurnList([
                            SpriteBackgroundStatusTurn(1),
                        ]),
                    ),
                    SpriteBackgroundStatus(
                        SpriteBackgroundStatusSrc('sample_sprite_pomeranians.png'),
                        SpriteBackgroundStatusSpriteSheetColumns(12),
                        SpriteBackgroundStatusSpriteSheetRows(8),
                        SpriteBackgroundStatusRangeBeginX(1),
                        SpriteBackgroundStatusRangeBeginY(1),
                        SpriteBackgroundStatusRangeEndX(3),
                        SpriteBackgroundStatusRangeEndY(1),
                        SpriteBackgroundStatusSituation(Situation.walkForward),
                        SpriteBackgroundStatusDuration(1000),
                        SpriteBackgroundStatusTurnList([
                            SpriteBackgroundStatusTurn(1),
                            SpriteBackgroundStatusTurn(0),
                            SpriteBackgroundStatusTurn(1),
                            SpriteBackgroundStatusTurn(2),
                        ]),
                    ),
                    SpriteBackgroundStatus(
                        SpriteBackgroundStatusSrc('sample_sprite_pomeranians.png'),
                        SpriteBackgroundStatusSpriteSheetColumns(12),
                        SpriteBackgroundStatusSpriteSheetRows(8),
                        SpriteBackgroundStatusRangeBeginX(1),
                        SpriteBackgroundStatusRangeBeginY(2),
                        SpriteBackgroundStatusRangeEndX(3),
                        SpriteBackgroundStatusRangeEndY(2),
                        SpriteBackgroundStatusSituation(Situation.walkLeft),
                        SpriteBackgroundStatusDuration(200),
                        SpriteBackgroundStatusTurnList([
                            SpriteBackgroundStatusTurn(1),
                            SpriteBackgroundStatusTurn(0),
                            SpriteBackgroundStatusTurn(1),
                            SpriteBackgroundStatusTurn(2),
                        ]),
                    ),
                    SpriteBackgroundStatus(
                        SpriteBackgroundStatusSrc('sample_sprite_pomeranians.png'),
                        SpriteBackgroundStatusSpriteSheetColumns(12),
                        SpriteBackgroundStatusSpriteSheetRows(8),
                        SpriteBackgroundStatusRangeBeginX(1),
                        SpriteBackgroundStatusRangeBeginY(3),
                        SpriteBackgroundStatusRangeEndX(3),
                        SpriteBackgroundStatusRangeEndY(3),
                        SpriteBackgroundStatusSituation(Situation.walkRight),
                        SpriteBackgroundStatusDuration(100),
                        SpriteBackgroundStatusTurnList([
                            SpriteBackgroundStatusTurn(1),
                            SpriteBackgroundStatusTurn(0),
                            SpriteBackgroundStatusTurn(1),
                            SpriteBackgroundStatusTurn(2),
                        ]),
                    ),
                    SpriteBackgroundStatus(
                        SpriteBackgroundStatusSrc('sample_sprite_pomeranians.png'),
                        SpriteBackgroundStatusSpriteSheetColumns(12),
                        SpriteBackgroundStatusSpriteSheetRows(8),
                        SpriteBackgroundStatusRangeBeginX(1),
                        SpriteBackgroundStatusRangeBeginY(4),
                        SpriteBackgroundStatusRangeEndX(3),
                        SpriteBackgroundStatusRangeEndY(4),
                        SpriteBackgroundStatusSituation(Situation.walkBackward),
                        SpriteBackgroundStatusDuration(1000),
                        SpriteBackgroundStatusTurnList([
                            SpriteBackgroundStatusTurn(1),
                            SpriteBackgroundStatusTurn(0),
                            SpriteBackgroundStatusTurn(1),
                            SpriteBackgroundStatusTurn(2),
                        ]),
                    ),
                ]),
                null,
                null,
            )),
        ]);
        store.actorSet = MutableActorSet([
            MutableActor.broadcast(Actor(
                ActorIdentifier(),
                ActorPositionX(0),
                ActorPositionY(0),
                ActorPositionZ(0),
                ActorWidth(0),
                ActorHeight(0),
                ActorDepth(0)
            )),
        ]);
    }
    final Store store;
    final Operate operate;

    late MutableWindowConfiguration windowConf;
    late MutableActor actor;

    var flag = true;
    void auto(MutableActor actor, int addition, int max, int mini) {
        if(flag) {
            actor.addNextPositionX(ActorPositionX(addition));
            if(actor.value.positionX.value >= max) flag = false;
        } else {
            actor.subtractNextPositionX(ActorPositionX(addition));
            if(actor.value.positionX.value <= mini) flag = true;
        }
    }

    @override
    void ngOnInit() {
        printd('root');
        windowConf = store.windowConfigurationSet.first;
        final id = store.createActorWithWindow(windowConf.value.windowStatus.identifier, broadcast: true);
        actor = store.actorSet.getById(id);

        final count = 100;
        for (var i = 0; i < count; i++) {
            store.createActorWithWindow(windowConf.identifier, broadcast: true);
        }
        printd('store.windowConfigurationSet.length',store.windowConfigurationSet.length);
        var index = 1;
        store.actorSet.forEach((element){
            if(element.identifier == actor.identifier) return;
            element.subtractNextPositionY(ActorPositionY(index));
            index = index + 10;
        });        

        final scale = 64;
        var right = 'right';
        var left = 'left';
        var up = 'up';
        var down = 'down';
        var direction = down; // 方向
        var nextPoint = 0;
        var onTheWay = false; // 向かってる途中
        final addition = 5;
        final max = 500;
        final mini = 0;
        operate.onDraw(() {
            var counter = 1;
            store.actorSet.forEach((element){
                if(element.identifier == actor.identifier) return;
                auto(element, counter, max, mini);
                counter++;
                if(counter >= 15) counter = 1;
            });

            int arrowRightNextPoint() {
                if(actor.value.positionX.value < scale) return scale;
                return (actor.value.positionX.value ~/ scale +1) * scale;
            }
            int arrowLeftNextPoint() {
                if(actor.value.positionX.value < scale) return 0;
                return (actor.value.positionX.value ~/ scale -1) * scale;
            }
            int arrowUpNextPoint() {
                if(actor.value.positionY.value < scale) return scale;
                return (actor.value.positionY.value ~/ scale +1) * scale;
            }
            int arrowDownNextPoint() {
                if(actor.value.positionY.value < scale) return 0;
                return (actor.value.positionY.value ~/ scale -1) * scale;
            }
            /// onTheWay が false になり、切り替わった時、動きがかくつく。
            if(onTheWay) {
                if(direction == right) {
                    if(actor.value.positionX.value + addition > nextPoint) {
                        if(operate.arrowRight) {
                            nextPoint = arrowRightNextPoint();
                            actor.addNextPositionX(ActorPositionX(addition));
                        } else {
                            actor.nextPositionX(ActorPositionX(nextPoint));
                            onTheWay = false;
                        }
                    } else {
                        actor.addNextPositionX(ActorPositionX(addition));
                    }
                } else if(direction == left) {
                    if(actor.value.positionX.value - addition < nextPoint) {
                        if(operate.arrowLeft) {
                            nextPoint = arrowLeftNextPoint();
                            actor.subtractNextPositionX(ActorPositionX(addition));
                        } else {
                            actor.nextPositionX(ActorPositionX(nextPoint));
                            onTheWay = false;
                        }
                    } else {
                        actor.subtractNextPositionX(ActorPositionX(addition));
                    }
                } else if(direction == up ) {
                    if(actor.value.positionY.value + addition > nextPoint) {
                        if(operate.arrowUp) {
                            nextPoint = arrowUpNextPoint();
                            actor.addNextPositionY(ActorPositionY(addition));
                        } else {
                            actor.nextPositionY(ActorPositionY(nextPoint));
                            onTheWay = false;
                        }
                    } else {
                        actor.addNextPositionY(ActorPositionY(addition));
                    }
                } else if(direction == down) {
                    if(actor.value.positionY.value - addition < nextPoint) {
                        if(operate.arrowDown) {
                            nextPoint = arrowDownNextPoint();
                            actor.subtractNextPositionY(ActorPositionY(addition));
                        } else {
                            actor.nextPositionY(ActorPositionY(nextPoint));
                            onTheWay = false;
                        }
                    } else {
                        actor.subtractNextPositionY(ActorPositionY(addition));
                    }
                }
            } else {
                if(operate.arrowRight) {
                    nextPoint = arrowRightNextPoint();
                    onTheWay = true;
                    direction = right;
                    windowConf.next(windowConf.value.changeWindowStatus(windowConf.value.windowStatus.changeSpriteSituation(SpriteBackgroundStatusSituation(Situation.walkRight))));
                } else if(operate.arrowLeft) {
                    nextPoint = arrowLeftNextPoint();
                    direction = left;
                    onTheWay = true;
                    windowConf.next(windowConf.value.changeWindowStatus(windowConf.value.windowStatus.changeSpriteSituation(SpriteBackgroundStatusSituation(Situation.walkLeft))));
                } else if(operate.arrowUp) {
                    nextPoint = arrowUpNextPoint();
                    direction = up;
                    onTheWay = true;
                    windowConf.next(windowConf.value.changeWindowStatus(windowConf.value.windowStatus.changeSpriteSituation(SpriteBackgroundStatusSituation(Situation.walkBackward))));
                } else if(operate.arrowDown) {
                    nextPoint = arrowDownNextPoint();
                    direction = down;
                    onTheWay = true;
                    windowConf.next(windowConf.value.changeWindowStatus(windowConf.value.windowStatus.changeSpriteSituation(SpriteBackgroundStatusSituation(Situation.walkForward))));
                }
            }
        });


    }

}