
import 'dart:html';

import 'package:angular/angular.dart';
import 'package:pub_browser_dependent_model/css_property.dart' hide Duration;
import 'package:pub_base_model/directive.dart' as base;
import 'package:sample_dart_web_angular/src/ayu/status/image_background/image_background_status.dart';

import 'image_background_process.dart';

import 'image_background_process_switch.template.dart' as temp;

@Component(
    selector: 'ayu-image-background-switch',
    templateUrl: 'image_background_process_switch.html',
    directives: [
        NgFor
        ,NgSwitch
        ,NgSwitchWhen
        ,ImageBackgroundProcess
    ],
    exports: [
    ],
)
class ImageBackgroundProcessSwitch
    extends
        base.Directive
    implements
        OnInit
{

    static final template = temp.ImageBackgroundProcessSwitchNgFactory;

    ImageBackgroundProcessSwitch(this.el);
    final HtmlElement el;

    @ViewChild('switchContainer')
    HtmlElement? switchContainer;

    @Input()
    late ImageBackgroundStatusList statusList;

    Object situation = 'initial';

    @override
    void ngOnInit() {
        if(switchContainer == null) return;
        switchContainer!.style.position = Position.absolute().toString();
        switchContainer!.style.top = '0';
        switchContainer!.style.bottom = '0';
        switchContainer!.style.left = '0';
        switchContainer!.style.right = '0';
    }

}