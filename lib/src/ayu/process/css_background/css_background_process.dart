
import 'dart:html';

import 'package:angular/angular.dart';
import 'package:pub_browser_dependent_model/css_property.dart';
import 'package:pub_base_model/directive.dart' as base;
import 'package:sample_dart_web_angular/src/ayu/status/css_background/css_background_status.dart';

import 'css_background_process.template.dart' as temp;

export 'css_background_process_switch.dart';

@Component(
    selector: 'ayu-css-background',
    template: '',
    directives: [
    ],
    exports: [
    ],
)
class CssBackgroundProcess
    extends
        base.Directive
    implements
        OnInit
        ,AfterViewInit
{

    bool get debugMode => true;

    static final template = temp.CssBackgroundProcessNgFactory;

    CssBackgroundProcess(this.el);
    final HtmlElement el;

    @Input()
    late CssBackgroundStatus status;

    @override
    void ngOnInit() {
        el.style.position = Position.absolute().toString();
        // el.style.position = Position.relative().toString();
        el.style.top = '0';
        el.style.bottom = '0';
        el.style.left = '0';
        el.style.right = '0';
        // el.style.width = '100%';
        // el.style.height = '100%';
    }

    @override
    void ngAfterViewInit() {
        cssBackgroundProcess();
        cssBorderProcess();
    }

    void cssBorderProcess () {
        if(
            status.borderColor != null &&
            status.borderStyle != null &&
            status.borderWidth != null
        ) {
            el.style.outline = '${status.borderWidth!.px} ${status.borderStyle!.value} ${status.borderColor!.value}';
            el.style.outlineOffset = '-${status.borderWidth!.px}';
        }
    }

    void cssBackgroundProcess () {

        switch (status.runtimeType) {
            case OneColorStatus:
                _oneColor(el, status as OneColorStatus); break;
            case Windows95Status:
                _windows95(el, status as Windows95Status); break;
            case DotsStatus:
                _dots(el, status as DotsStatus); break;
            case CrossDotsStatus:
                _crossDots(el, status as CrossDotsStatus); break;
            case ChecksStatus:
                _checks(el, status as ChecksStatus); break;
            default:
                print('$runtimeType: cssBackgroundProcess() 設定がされていない css background status です。 ${status.runtimeType}');
        }

    }

    HtmlElement _oneColor(HtmlElement el, OneColorStatus status){
        el.style.backgroundColor = status.color.toString();
        return el;
    }

    /// Windows 95 background in CSS: "https://codepen.io/bennettfeely/pen/MWbgEvX"
    HtmlElement _windows95(HtmlElement el, Windows95Status status){
        el.style.background = '''
            conic-gradient(
                from 45deg,
                #020681 0,
                #020681 90deg,
                #000337 90deg,
                #000337 180deg,
                #0818ff 180deg,
                #0818ff 270deg,
                #0f9bfe 270deg,
                #0f9bfe 360deg
            )
        ''';
        el.style.backgroundSize = '${status.size.px} ${status.size.px}';
        return el;
    }

   HtmlElement _dots(HtmlElement el, DotsStatus status){
        el.style.backgroundColor = status.backgroundColor.toString();
        el.style.backgroundImage = 'radial-gradient(${status.dotsColor.toString()} ${status.size.px}, transparent 1px)';
        el.style.backgroundSize = 'calc(10 * ${status.interval.px}) calc(10 * ${status.interval.px})';
        return el;
    }

   HtmlElement _crossDots(HtmlElement el, CrossDotsStatus status){
        el.style.backgroundColor = status.backgroundColor.toString();
        el.style.backgroundImage = '''
            radial-gradient(${status.dotsColor.toString()} ${status.size.px}, transparent 1px),
            radial-gradient(${status.dotsColor.toString()} ${status.size.px}, transparent 1px)
        ''';
        el.style.backgroundSize = 'calc(20 * ${status.interval.px}) calc(20 * ${status.interval.px})';
        el.style.backgroundPosition = '0 0, calc(10 * ${status.interval.px}) calc(10 * ${status.interval.px})';
        return el;
    }

   HtmlElement _checks(HtmlElement el, ChecksStatus status){
        el.style.backgroundImage = '''
            repeating-linear-gradient(
                45deg,
                ${status.checksColor} 25%,
                transparent 25%,
                transparent 75%,
                ${status.checksColor} 75%,
                ${status.checksColor}
            ),
            repeating-linear-gradient(
                45deg,
                ${status.checksColor} 25%,
                transparent 25%,
                transparent 75%,
                ${status.checksColor} 75%,
                ${status.checksColor}
            )
        ''';
        el.style.backgroundPosition = '0 0, ${status.size.px} ${status.size.px}';
        el.style.backgroundSize = 'calc(2 * ${status.size.px}) calc(2 * ${status.size.px})';
        el.style.backgroundColor = status.backgroundColor.toString();
        return el;
    }

}