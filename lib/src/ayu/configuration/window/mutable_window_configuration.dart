
import 'package:sample_dart_web_angular/src/ayu/status/window/window_status_barrel.dart';
import 'package:sample_dart_web_angular/src/sample_dart_web_angular_module.dart';
import 'package:pub_base_model/material.dart';
import 'package:pub_base_model/unique_object_model.dart';
import 'package:pub_debug_model/debug_model.dart';

import 'window_configuration.dart';

class MutableWindowConfiguration
    extends
        NonNullableMutable<WindowConfiguration>
    implements
        ObjectModel<SampleDartWebAngularModule>
        ,UniqueObjectModel<
            WindowStatusIdentifier
            ,SampleDartWebAngularModule
        >
{

    MutableWindowConfiguration(WindowConfiguration value) : super(value);

    @override
    String operator [](String key) {
        if(toPrimitive().containsKey(key)) return toPrimitive()[key] as String;
        throw ProductError(
            SampleDartWebAngularModule,
            runtimeType,
            [
                'error の発生場所 => $runtimeType.operator []',
                '入力した key => $key ',
                '有効な key の一覧 => ${toPrimitive().keys}',
            ]
        );
    }

    @override
    WindowStatusIdentifier get identifier => value.windowStatus.identifier;

    @override
    Map<String, String> toPrimitive() => value.toPrimitive();

}