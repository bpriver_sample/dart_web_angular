
import 'package:sample_dart_web_angular/src/ayu/status/status_barrel.dart';
import 'package:sample_dart_web_angular/src/sample_dart_web_angular_module.dart';
import 'package:pub_base_model/model.dart';

export 'mutable_window_configuration.dart';
export 'mutable_window_configuration_set.dart';

class WindowConfiguration
    extends
        ObjectModel<SampleDartWebAngularModule>
{

    WindowConfiguration(this.windowStatus, [this.spriteBackgroundStatusList,  this.cssBackgroundStatusList, this.imageBackgroundStatusList,]);
    
    final WindowStatus windowStatus;
    final SpriteBackgroundStatusList? spriteBackgroundStatusList;
    final CssBackgroundStatusList? cssBackgroundStatusList;
    final ImageBackgroundStatusList? imageBackgroundStatusList;

    WindowConfiguration changeWindowStatus(WindowStatus windowStatus) {
        return WindowConfiguration(windowStatus, spriteBackgroundStatusList,);
    }

    @override
    Map<String, String> toPrimitive() {
        return {
            'windowStatus': windowStatus.toString(),
            'spriteBackgroundStatusList': spriteBackgroundStatusList.toString(),
        };
    }

}