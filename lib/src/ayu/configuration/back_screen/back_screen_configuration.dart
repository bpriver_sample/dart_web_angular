
import 'package:sample_dart_web_angular/src/ayu/status/status_barrel.dart';
import 'package:sample_dart_web_angular/src/sample_dart_web_angular_module.dart';
import 'package:pub_base_model/model.dart';

class BackScreenConfiguration
    extends
        ObjectModel<SampleDartWebAngularModule>
{

    BackScreenConfiguration([this.cssBackgroundStatus]);

    final CssBackgroundStatus? cssBackgroundStatus;
    
    @override
    Map<String, String> toPrimitive() {
        return {
            'cssBackgroundStatus': cssBackgroundStatus.toString(),
        };
    }

}