
import 'dart:html';

import 'package:angular/angular.dart';
import 'package:pub_browser_dependent_model/css_property.dart';
import 'package:sample_dart_web_angular/src/ayu/configuration/configuration_barrel.dart';
import 'package:sample_dart_web_angular/src/ayu/process/process_layer.dart';
import 'package:pub_base_model/directive.dart' as base;

import 'back_screen_directive.template.dart' as temp;

@Component(
    selector: 'ayu-back-screen',
    templateUrl: 'back_screen_directive.html',
    directives: [
        NgIf
        ,CssBackgroundProcess
    ],
    exports: [
    ],
)
class BackScreenDirective
    extends
        base.Directive
    implements
        OnInit
{

    static final template = temp.BackScreenDirectiveNgFactory;

    BackScreenDirective(this.el);
    @override
    final HtmlElement el;

    @Input()
    late BackScreenConfiguration configuration;

    @override
    void ngOnInit() {
        /// ブラウザの大きさにキッチリ合うようにする。
        el.style.position = Position.fixed().toString();
        el.style.top = '0';
        el.style.bottom = '0';
        el.style.left = '0';
        el.style.right = '0';

        el.style.display = Display.flex().toString();
        el.style.justifyContent = JustifyContent.center().toString();
        el.style.alignItems = AlignItems.center().toString();
    }
 
}