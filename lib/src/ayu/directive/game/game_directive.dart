
import 'dart:html';

import 'package:angular/angular.dart';
import 'package:sample_dart_web_angular/src/ayu/operate/operate.dart';
import 'package:pub_base_model/directive.dart' as base;

import 'game_directive.template.dart' as temp;

@Component(
    selector: 'ayu-game',
    templateUrl: 'game_directive.html',
    directives: [
    ],
    providers: [
    ],
    exports: [
    ],
)
class GameDirective
    extends
        base.Directive
    implements
        OnInit
{

    static final template = temp.GameDirectiveNgFactory;

    GameDirective(this.el, this.operate);
    final HtmlElement el;
    final Operate operate;

    bool isRequestAnimationFrame = true;
    void stopRequestAnimationFrame() => isRequestAnimationFrame = false;
    void startRequestAnimationFrame() {
        isRequestAnimationFrame = true;
        printd('start requestAnimationFrame');
        window.requestAnimationFrame(render);
    }

    Future<void> render(num timestamp) async {
        if (isRequestAnimationFrame) {
            operate.draw();
            window.requestAnimationFrame(render);
        } else {
            printd('stop requestAnimationFrame');
        }
    }

    @override
    void ngOnInit() {
        /// カーソルを合わせれるようにすることで、keydown などの event を受け取ることができる。
        el.tabIndex = 0;
        // startRequestAnimationFrame();
    }

    @HostListener('keydown.arrowright')
    void arrowRightDown() => operate.arrowRight = true;
    @HostListener('keyup.arrowright')
    void arrowRightUp() => operate.arrowRight = false;

    @HostListener('keydown.arrowleft')
    void arrowLeftDown() => operate.arrowLeft  = true;
    @HostListener('keyup.arrowleft')
    void arrowLeftUp() => operate.arrowLeft  = false;

    @HostListener('keydown.arrowup')
    void arrowUpDown() => operate.arrowUp = true;
    @HostListener('keyup.arrowup')
    void arrowUpUp() => operate.arrowUp = false;

    @HostListener('keydown.arrowdown')
    void arrowDownDown() => operate.arrowDown = true;
    @HostListener('keyup.arrowdown')
    void arrowDownUp() => operate.arrowDown = false;

    @HostListener('keydown.space')
    void spaceDown() => operate.space = true;
    @HostListener('keyup.space')
    // void spaceUp() => operate.space = false;
    void spaceUp() {
        operate.space = false;
        stopRequestAnimationFrame();
    }

    @HostListener('keydown.enter')
    void enterDown() => operate.enter = true;
    @HostListener('keyup.enter')
    // void enterUp() => operate.enter = false;
    void enterUp() {
        operate.enter = false;
        startRequestAnimationFrame();
    }
}