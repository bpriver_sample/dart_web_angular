
import 'package:sample_dart_web_angular/src/sample_dart_web_angular_module.dart';
import 'package:pub_base_model/model.dart';

import 'height/viewport_status_height.dart';
import 'inside_height/viewport_status_inside_height.dart';
import 'inside_width/viewport_status_inside_width.dart';
import 'width/viewport_status_width.dart';


export 'height/viewport_status_height.dart';
export 'width/viewport_status_width.dart';
export 'inside_width/viewport_status_inside_width.dart';
export 'inside_height/viewport_status_inside_height.dart';

class ViewportStatus
    extends
        ObjectModel<SampleDartWebAngularModule>
{

    ViewportStatus(this.width, this.height, this.insideWidth, this.insideHeight);

    final ViewportStatusWidth width;
    final ViewportStatusHeight height;
    final ViewportStatusInsideWidth insideWidth;
    final ViewportStatusInsideHeight insideHeight;
    
    @override
    Map<String, String> toPrimitive() {
        return {
            'width': width.toString(),
            'height': height.toString(),
            'insideWidth': insideWidth.toString(),
            'insideHeight': insideHeight.toString(),
        };
    }

}