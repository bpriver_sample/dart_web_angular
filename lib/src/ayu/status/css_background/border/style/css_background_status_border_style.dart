
import 'package:pub_browser_dependent_model/css_property.dart';

class CssBackgroundStatusBorderStyle
    extends
        BorderStyle
{
    
    CssBackgroundStatusBorderStyle.none() : super.none();
    CssBackgroundStatusBorderStyle.hidden() : super.hidden();
    CssBackgroundStatusBorderStyle.solid() : super.solid();
    CssBackgroundStatusBorderStyle.double() : super.double();
    CssBackgroundStatusBorderStyle.groove() : super.groove();
    CssBackgroundStatusBorderStyle.ridge() : super.ridge();
    CssBackgroundStatusBorderStyle.inset() : super.inset();
    CssBackgroundStatusBorderStyle.outset() : super.outset();
    CssBackgroundStatusBorderStyle.dashed() : super.dashed();
    CssBackgroundStatusBorderStyle.dotted() : super.dotted();

}