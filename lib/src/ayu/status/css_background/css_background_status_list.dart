
import 'package:sample_dart_web_angular/src/sample_dart_web_angular_module.dart';
import 'package:pub_base_model/computation.dart';

import 'css_background_status.dart';

class CssBackgroundStatusList
    extends
        ListComputation<
            CssBackgroundStatus
            ,CssBackgroundStatusList
            ,SampleDartWebAngularModule
        >
{

    CssBackgroundStatusList(this.values);

    @override
    final Iterable<CssBackgroundStatus> values;
        
    @override
    CssBackgroundStatusList internalFactory(Iterable<CssBackgroundStatus> models) => CssBackgroundStatusList(models);

}
