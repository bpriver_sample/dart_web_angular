
import '../css_background_status.dart';

export 'size/windows_95_status_size.dart';

class Windows95Status
    extends
        CssBackgroundStatus
{

    Windows95Status(this.size, this.situation, [this.borderColor, this.borderWidth, this.borderStyle]);
    final Windows95StatusSize size;
    @override
    final CssBackgroundStatusSituation situation;
    @override
    final CssBackgroundStatusBorderColor? borderColor;
    @override
    final CssBackgroundStatusBorderWidth? borderWidth;
    @override
    final CssBackgroundStatusBorderStyle? borderStyle;

    @override
    Map<String, String> toPrimitive() {
        return {
            'size': size.toString(),
        }..addAll(super.toPrimitive());
    }

}