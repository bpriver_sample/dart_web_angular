
import '../css_background_status.dart';

export 'background_color/one_color_status_background_color.dart';

class OneColorStatus
    extends
        CssBackgroundStatus
{

    OneColorStatus(this.color, this.situation, [this.borderColor, this.borderWidth, this.borderStyle]);
    final OneColorStatusBackgroundColor color;
    @override
    final CssBackgroundStatusSituation situation;
    @override
    final CssBackgroundStatusBorderColor? borderColor;
    @override
    final CssBackgroundStatusBorderWidth? borderWidth;
    @override
    final CssBackgroundStatusBorderStyle? borderStyle;

    @override
    Map<String, String> toPrimitive() {
        return {
            'color': color.toString(),
        }..addAll(super.toPrimitive());
    }

}