
import 'package:pub_browser_dependent_model/css_property.dart';

class CrossDotsStatusDotsColor
    extends
        Color
{

    const CrossDotsStatusDotsColor.rgba(String value) : super.rgba(value);
    const CrossDotsStatusDotsColor.black() : super.black();
    const CrossDotsStatusDotsColor.blue() : super.blue();
    const CrossDotsStatusDotsColor.red() : super.red();
    const CrossDotsStatusDotsColor.purple() : super.purple();
    const CrossDotsStatusDotsColor.green() : super.green();
    const CrossDotsStatusDotsColor.yellow() : super.yellow();

}