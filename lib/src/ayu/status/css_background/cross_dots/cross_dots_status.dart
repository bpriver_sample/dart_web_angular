
import '../css_background_status.dart';

export 'size/cross_dots_status_size.dart';
export 'interval/cross_dots_status_interval.dart';
export 'background_color/cross_dots_status_background_color.dart';
export 'dots_color/cross_dots_status_dots_color.dart';

class CrossDotsStatus
    extends
        CssBackgroundStatus
{

    CrossDotsStatus(this.backgroundColor, this.dotsColor, this.size, this.interval, this.situation, [this.borderColor, this.borderWidth, this.borderStyle]);
    final CrossDotsStatusBackgroundColor backgroundColor;
    final CrossDotsStatusDotsColor dotsColor;
    final CrossDotsStatusSize size;
    final CrossDotsStatusInterval interval;
    @override
    final CssBackgroundStatusSituation situation;
    @override
    final CssBackgroundStatusBorderColor? borderColor;
    @override
    final CssBackgroundStatusBorderWidth? borderWidth;
    @override
    final CssBackgroundStatusBorderStyle? borderStyle;

    @override
    Map<String, String> toPrimitive() {
        return {
            'backgroundColor': backgroundColor.toString(),
            'dotsColor': dotsColor.toString(),
            'size': size.toString(),
            'interval': interval.toString(),
        }..addAll(super.toPrimitive());
    }

}