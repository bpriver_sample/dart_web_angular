
import 'package:pub_browser_dependent_model/css_property.dart';

class ChecksStatusChecksColor
    extends
        Color
{

    const ChecksStatusChecksColor.rgba(String value) : super.rgba(value);
    const ChecksStatusChecksColor.black() : super.black();
    const ChecksStatusChecksColor.blue() : super.blue();
    const ChecksStatusChecksColor.red() : super.red();
    const ChecksStatusChecksColor.purple() : super.purple();
    const ChecksStatusChecksColor.green() : super.green();
    const ChecksStatusChecksColor.yellow() : super.yellow();

}