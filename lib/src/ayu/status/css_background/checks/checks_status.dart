
import '../css_background_status.dart';

export 'size/checks_status_size.dart';
export 'background_color/checks_status_background_color.dart';
export 'checks_color/checks_status_checks_color.dart';

class ChecksStatus
    extends
        CssBackgroundStatus
{

    ChecksStatus(this.backgroundColor, this.checksColor, this.size, this.situation, [this.borderColor, this.borderWidth, this.borderStyle]);
    final ChecksStatusBackgroundColor backgroundColor;
    final ChecksStatusChecksColor checksColor;
    final ChecksStatusSize size;
    @override
    final CssBackgroundStatusSituation situation;
    @override
    final CssBackgroundStatusBorderColor? borderColor;
    @override
    final CssBackgroundStatusBorderWidth? borderWidth;
    @override
    final CssBackgroundStatusBorderStyle? borderStyle;

    @override
    Map<String, String> toPrimitive() {
        return {
            'backgroundColor': backgroundColor.toString(),
            'checksColor': checksColor.toString(),
            'size': size.toString(),
        }..addAll(super.toPrimitive());
    }

}