
import 'package:pub_base_model/model.dart';
import 'package:sample_dart_web_angular/src/sample_dart_web_angular_module.dart';

import 'border/css_background_status_border_barrel.dart';
import 'situation/css_background_status_situation.dart';

export 'css_background_status_list.dart';
export 'checks/checks_status.dart';
export 'cross_dots/cross_dots_status.dart';
export 'dots/dots_status.dart';
export 'one_color/one_color._status.dart';
export 'windows_95/windows_95_status.dart';

export 'border/css_background_status_border_barrel.dart';
export 'windows_95/windows_95_status.dart';
export 'dots/dots_status.dart';
export 'cross_dots/cross_dots_status.dart';
export 'checks/checks_status.dart';
export 'situation/css_background_status_situation.dart';
export 'css_background_status.dart';

abstract class CssBackgroundStatus
    extends
        ObjectModel<SampleDartWebAngularModule>
{
    
    CssBackgroundStatusSituation get situation;
    CssBackgroundStatusBorderColor? get borderColor;
    CssBackgroundStatusBorderWidth? get borderWidth;
    CssBackgroundStatusBorderStyle? get borderStyle;

    @override
    Map<String, String> toPrimitive() {
        return {
            'situation': situation.toString(),
            'borderColor': borderColor.toString(),
            'borderWidth': borderWidth.toString(),
            'borderStyle': borderStyle.toString(),
        };
    }

}