
import '../window_status_barrel.dart';

class WindowStatusWidth
    extends
        WindowStatusBarrel<
            WindowStatusWidth
        >
{

    /// 0 から 10000 までの整数（int 型）
    /// 単位は px.
    WindowStatusWidth(this.value);

    @override
    final int value;

    @override
    WindowStatusWidth internalFactory(int value) => WindowStatusWidth(value);

}