
import 'package:sample_dart_web_angular/src/sample_dart_web_angular_module.dart';
import 'package:pub_base_model/identifier.dart';

export 'window_status_identifier_list.dart';

class WindowStatusIdentifier
    extends
        Identifier<
            WindowStatusIdentifier
            ,SampleDartWebAngularModule
        >
{
    
    WindowStatusIdentifier() : super.implicit();

}