
import 'package:sample_dart_web_angular/src/sample_dart_web_angular_module.dart';
import 'package:pub_base_model/computation.dart';

import 'window_status.dart';

class WindowStatusList
    extends
        ListComputation<
            WindowStatus
            ,WindowStatusList
            ,SampleDartWebAngularModule
        >
{

    WindowStatusList(this.values);

    @override
    final Iterable<WindowStatus> values;

    @override
    WindowStatusList internalFactory(Iterable<WindowStatus> models) => WindowStatusList(models);

}