
import '../window_status_barrel.dart';

class WindowStatusYAxis
    extends
        WindowStatusBarrel<
            WindowStatusYAxis
        >
{

    /// 0 から 10000 までの整数（int 型）
    /// 単位は px.    
    WindowStatusYAxis(this.value);

    @override
    final int value;

    @override
    WindowStatusYAxis internalFactory(int value) => WindowStatusYAxis(value);

}