
import '../window_status_barrel.dart';

class WindowStatusHeight
    extends
        WindowStatusBarrel<
            WindowStatusHeight
        >
{

    /// 0 から 10000 までの整数（int 型）
    /// 単位は px.    
    WindowStatusHeight(this.value);

    @override
    final int value;

    @override
    WindowStatusHeight internalFactory(int value) => WindowStatusHeight(value);

}