
import '../window_status_barrel.dart';

class WindowStatusPaddingLeft
    extends
        WindowStatusBarrel<
            WindowStatusPaddingLeft
        >
{

    /// 0 から 10000 までの整数（int 型）
    /// 単位は px.    
    WindowStatusPaddingLeft(this.value);

    @override
    final int value;

    @override
    WindowStatusPaddingLeft internalFactory(int value) => WindowStatusPaddingLeft(value);

}