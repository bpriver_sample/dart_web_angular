
import '../window_status_barrel.dart';

class WindowStatusPaddingBottom
    extends
        WindowStatusBarrel<
            WindowStatusPaddingBottom
        >
{

    /// 0 から 10000 までの整数（int 型）
    /// 単位は px.    
    WindowStatusPaddingBottom(this.value);

    @override
    final int value;

    @override
    WindowStatusPaddingBottom internalFactory(int value) => WindowStatusPaddingBottom(value);

}