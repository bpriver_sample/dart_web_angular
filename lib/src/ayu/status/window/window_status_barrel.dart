
import 'package:sample_dart_web_angular/src/sample_dart_web_angular_module.dart';
import 'package:pub_base_model/between_x_and_y.dart';
import 'package:pub_base_model/model.dart';
import 'package:pub_base_model/num_state_model_action.dart';

export 'identifier/window_status_identifier.dart';
export 'height/window_status_height.dart';
export 'width/window_status_width.dart';
export 'x_axis/window_status_x_axis.dart';
export 'y_axis/window_status_y_axis.dart';
export 'padding_top/window_status_padding_top.dart';
export 'padding_bottom/window_status_padding_bottom.dart';
export 'padding_left/window_status_padding_left.dart';
export 'padding_right/window_status_padding_right.dart';

/// 
/// MS・・・MySelf.
/// AM・・・ArgumentModel.
/// 
/// 0 から 10000 までの整数（int 型）
/// 単位は無し。
/// 
/// with するのが大変なので、意味をもたない barrel を base class として利用。
/// 
abstract class WindowStatusBarrel<
        MS extends WindowStatusBarrel<MS>
    >
    extends
        IntStateModel
    with
        BetweenXAndY<
            int
            ,MS
            ,SampleDartWebAngularModule
        >
        ,Between0And10000<
            int
            ,MS
            ,SampleDartWebAngularModule
        >
        ,NumStateModelGetPercent<
            int
        >
        ,NumStateModelThanOperator<
            int
            ,MS
        >
        ,BetweenXAndYOperator<
            int
            ,MS
            ,SampleDartWebAngularModule
        >
        ,NumStateModelGetPx<
            int
        >
{

    WindowStatusBarrel() {

        validationBetweenXAndY();
        
    }   

}