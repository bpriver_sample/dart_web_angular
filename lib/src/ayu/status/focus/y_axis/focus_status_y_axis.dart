
import 'package:pub_base_model/between_x_and_y.dart';
import 'package:pub_base_model/model.dart';
import 'package:pub_base_model/num_state_model_action.dart';
import 'package:sample_dart_web_angular/src/sample_dart_web_angular_module.dart';

class FocusStatusYAxis
    extends
        IntStateModel
    with
        BetweenXAndY<
            int
            ,FocusStatusYAxis
            ,SampleDartWebAngularModule
        >
        ,Between0And10000<
            int
            ,FocusStatusYAxis
            ,SampleDartWebAngularModule
        >
        ,NumStateModelGetPx<
            int
        >
{

    /// 0 から 10000 までの整数（int 型）
    /// 単位は px.    
    FocusStatusYAxis(this.value);

    @override
    final int value;

}