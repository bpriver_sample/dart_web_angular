
import 'package:pub_base_model/model.dart';

class ImageBackgroundStatusXAxisRepeat
    extends
        StringStateModel
{

    /// x軸方向に繰り返す
    ImageBackgroundStatusXAxisRepeat.on()
    : value = 'repeat';

    /// x軸方向に切り抜きなしで敷き詰められる回数だけ繰り返される。
    /// 最初と最後は領域の両端にそれぞれ接触するように描画される。
    /// 余白は均一に分配される。
    ImageBackgroundStatusXAxisRepeat.space()
    : value = 'space';

    /// x軸方向に隙間を空けずに、もう1つ追加するだけの余裕 (残りの空間 ≧ 画像の幅の半分) ができるまで繰り返される。
    /// すべてが収まるように縮小される。
    ImageBackgroundStatusXAxisRepeat.round()
    : value = 'round';

    /// 繰り返さない
    ImageBackgroundStatusXAxisRepeat.off()
    : value = 'no-repeat';
    
    @override
    final String value;

}