
import 'package:sample_dart_web_angular/src/sample_dart_web_angular_module.dart';
import 'package:pub_base_model/computation.dart';

import 'image_background_status.dart';

class ImageBackgroundStatusList
    extends
        ListComputation<
            ImageBackgroundStatus
            ,ImageBackgroundStatusList
            ,SampleDartWebAngularModule
        >
{

    ImageBackgroundStatusList(this.values);

    @override
    final Iterable<ImageBackgroundStatus> values;
        
    @override
    ImageBackgroundStatusList internalFactory(Iterable<ImageBackgroundStatus> models) => ImageBackgroundStatusList(models);

}
