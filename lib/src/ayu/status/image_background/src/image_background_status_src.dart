
import 'package:pub_base_model/model.dart';

class ImageBackgroundStatusSrc
    extends
        StringStateModel
{

    /// path を指定。
    ImageBackgroundStatusSrc(this.value);

    @override
    final String value;

}