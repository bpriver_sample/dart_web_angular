
import 'package:pub_base_model/mutable.dart';

import 'image_background_status_situation.dart';

class MutableImageBackgroundStatusSituation
    extends
        NonNullableMutable<ImageBackgroundStatusSituation>
{

    MutableImageBackgroundStatusSituation(ImageBackgroundStatusSituation value) : super(value);

}