
import 'package:pub_base_model/model.dart';

export 'mutable_image_background_status_situation.dart';

class ImageBackgroundStatusSituation
    extends
        StateModel<Object>
{

    const ImageBackgroundStatusSituation.initial()
    : value = 'initial';

    const ImageBackgroundStatusSituation(this.value);

    @override
    final Object value;

}