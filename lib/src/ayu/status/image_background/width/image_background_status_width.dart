
import 'package:sample_dart_web_angular/src/sample_dart_web_angular_module.dart';
import 'package:pub_base_model/between_x_and_y.dart';
import 'package:pub_base_model/model.dart';
import 'package:pub_base_model/num_state_model_action.dart';

class ImageBackgroundStatusWidth
    extends
        IntStateModel
    with
        BetweenXAndY<
            int
            ,ImageBackgroundStatusWidth
            ,SampleDartWebAngularModule
        >
        ,Between0And10000<
            int
            ,ImageBackgroundStatusWidth
            ,SampleDartWebAngularModule
        >
        ,NumStateModelGetPx<
            int
        >
{

    /// 縦横比を保つ大きさになる。
    /// width と height 両方とも auto に指定した場合、原寸大になる。
    ImageBackgroundStatusWidth.auto()
    : value = 0 {
        validationBetweenXAndY();
    }

    ImageBackgroundStatusWidth(this.value) {
        validationBetweenXAndY();
    }

    @override
    final int value;

}