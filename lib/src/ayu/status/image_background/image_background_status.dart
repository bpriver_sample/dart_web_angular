
import 'package:pub_base_model/model.dart';
import 'package:sample_dart_web_angular/src/sample_dart_web_angular_module.dart';

import 'height/image_background_status_height.dart';
import 'situation/image_background_status_situation.dart';
import 'size/image_background_status_size.dart';
import 'src/image_background_status_src.dart';
import 'width/image_background_status_width.dart';
import 'x_axis_repeat/image_background_status_x_axis_repeat.dart';
import 'y_axis_repeat/image_background_status_y_axis_repeat.dart';

export 'src/image_background_status_src.dart';
export 'size/image_background_status_size.dart';
export 'x_axis_repeat/image_background_status_x_axis_repeat.dart';
export 'y_axis_repeat/image_background_status_y_axis_repeat.dart';
export 'width/image_background_status_width.dart';
export 'height/image_background_status_height.dart';
export 'size/image_background_status_size.dart';
export 'situation/image_background_status_situation.dart';
export 'image_background_status.dart';

export 'image_background_status_list.dart';

class ImageBackgroundStatus
    extends
        ObjectModel<SampleDartWebAngularModule>
{

    // size を cover などの keyword で指定する場合、repeat はしない。
    ImageBackgroundStatus.autoSize(this.src, this.size, this.situation)
    : 
        width = null
        ,height = null
        ,xAxisRepeat = ImageBackgroundStatusXAxisRepeat.off()
        ,yAxisRepeat = ImageBackgroundStatusYAxisRepeat.off()
    ;

    ImageBackgroundStatus.pxSize(this.src, this.width, this.height, this.xAxisRepeat, this.yAxisRepeat, this.situation)
    : 
        size = null
    ;
    
    final ImageBackgroundStatusSrc src;
    final ImageBackgroundStatusSize? size;
    final ImageBackgroundStatusWidth? width;
    final ImageBackgroundStatusHeight? height;
    final ImageBackgroundStatusXAxisRepeat xAxisRepeat;
    final ImageBackgroundStatusYAxisRepeat yAxisRepeat;
    final ImageBackgroundStatusSituation situation;

    @override
    Map<String, String> toPrimitive() {
        return {
            'src': src.toString(),
            'size': size.toString(),
            'width': width.toString(),
            'height': height.toString(),
            'xAxisRepeat': xAxisRepeat.toString(),
            'yAxisRepeat': yAxisRepeat.toString(),
            'situation': situation.toString(),
        };
    }

}