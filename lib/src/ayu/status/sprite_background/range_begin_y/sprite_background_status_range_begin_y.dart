
import 'package:pub_base_model/model.dart';

class SpriteBackgroundStatusRangeBeginY
    extends
        IntStateModel
{

    SpriteBackgroundStatusRangeBeginY(this.value);

    @override
    final int value;

}