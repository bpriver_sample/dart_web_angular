
import 'package:sample_dart_web_angular/src/sample_dart_web_angular_module.dart';
import 'package:pub_base_model/model.dart';

import 'duration/sprite_background_status_duration.dart';
import 'range_begin_x/sprite_background_status_range_begin_x.dart';
import 'range_begin_y/sprite_background_status_range_begin_y.dart';
import 'range_end_x/sprite_background_status_range_end_x.dart';
import 'range_end_y/sprite_background_status_range_end_y.dart';
import 'situation/sprite_background_status_situation.dart';
import 'sprite_sheet_columns/sprite_background_status_sprite_sheet_columns.dart';
import 'sprite_sheet_rows/sprite_background_status_sprite_sheet_rows.dart';
import 'src/sprite_background_status_src.dart';
import 'turn/sprite_background_status_turn.dart';

export 'sprite_background_status_list.dart';
export 'src/sprite_background_status_src.dart';
export 'sprite_sheet_columns/sprite_background_status_sprite_sheet_columns.dart';
export 'sprite_sheet_rows/sprite_background_status_sprite_sheet_rows.dart';
export 'range_begin_x/sprite_background_status_range_begin_x.dart';
export 'range_begin_y/sprite_background_status_range_begin_y.dart';
export 'range_end_x/sprite_background_status_range_end_x.dart';
export 'range_end_y/sprite_background_status_range_end_y.dart';
export 'situation/sprite_background_status_situation.dart';
export 'duration/sprite_background_status_duration.dart';
export 'turn/sprite_background_status_turn.dart';

class SpriteBackgroundStatus
    extends
        ObjectModel<SampleDartWebAngularModule>
{


    SpriteBackgroundStatus(this.src, this.spriteSheetColumns, this.spriteSheetRows, this.rangeBeginX, this.rangeBeginY, this.rangeEndX, this.rangeEndY, this.situation, this.duration, this.turnList);
    final SpriteBackgroundStatusSrc src;
    final SpriteBackgroundStatusSpriteSheetColumns spriteSheetColumns; 
    final SpriteBackgroundStatusSpriteSheetRows spriteSheetRows;
    final SpriteBackgroundStatusRangeBeginX rangeBeginX;
    final SpriteBackgroundStatusRangeBeginY rangeBeginY;
    final SpriteBackgroundStatusRangeEndX rangeEndX;
    final SpriteBackgroundStatusRangeEndY rangeEndY;
    final SpriteBackgroundStatusSituation situation;
    final SpriteBackgroundStatusDuration duration;
    final SpriteBackgroundStatusTurnList turnList;

    @override
    Map<String, String> toPrimitive() {
        return {
            'src': src.toString(),
            'spriteSheetColumns': spriteSheetColumns.toString(),
            'spriteSheetRows': spriteSheetRows.toString(),
            'rangeBeginX': rangeBeginX.toString(),
            'rangeBeginY': rangeBeginY.toString(),
            'rangeEndX': rangeEndX.toString(),
            'rangeEndY': rangeEndY.toString(),
            'situation': situation.toString(),
            'duration': duration.toString(),
            'turnList': turnList.toString(),
        };
    }

}