
import 'package:pub_base_model/model.dart';

class SpriteBackgroundStatusSrc
    extends
        StringStateModel
{

    /// path を指定。
    SpriteBackgroundStatusSrc(this.value);

    @override
    final String value;

}