
import 'package:pub_base_model/model.dart';

export 'mutable_sprite_background_status_situation.dart';

class SpriteBackgroundStatusSituation
    extends
        StateModel<Object>
{

    const SpriteBackgroundStatusSituation.initial()
    : value = 'initial';

    const SpriteBackgroundStatusSituation(this.value);

    @override
    final Object value;

}