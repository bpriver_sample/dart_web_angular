
import 'package:pub_base_model/model.dart';

class SpriteBackgroundStatusSpriteSheetRows
    extends
        IntStateModel
{

    SpriteBackgroundStatusSpriteSheetRows(this.value);

    @override
    final int value;

}