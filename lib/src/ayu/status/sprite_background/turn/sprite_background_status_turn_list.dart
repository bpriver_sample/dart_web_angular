
import 'package:sample_dart_web_angular/src/sample_dart_web_angular_module.dart';
import 'package:pub_base_model/computation.dart';

import 'sprite_background_status_turn.dart';

/// list の数に制限はないが、識字は、sprite animation の particle 数 - 1 の範囲内で指定する。
/// 左上から右下に向かって、Z の順番に、0 から始まる。
class SpriteBackgroundStatusTurnList
    extends
        ListComputation<
            SpriteBackgroundStatusTurn
            ,SpriteBackgroundStatusTurnList
            ,SampleDartWebAngularModule
        >
{

    SpriteBackgroundStatusTurnList(this.values);

    @override
    final Iterable<SpriteBackgroundStatusTurn> values;
    
    @override
    SpriteBackgroundStatusTurnList internalFactory(Iterable<SpriteBackgroundStatusTurn> models) => SpriteBackgroundStatusTurnList(models);

}