
import 'package:pub_base_model/model.dart';

class SpriteBackgroundStatusSpriteSheetColumns
    extends
        IntStateModel
{

    SpriteBackgroundStatusSpriteSheetColumns(this.value);

    @override
    final int value;

}