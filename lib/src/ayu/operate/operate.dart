
import 'dart:async';

import 'package:pub_base_model/material.dart';

class Operate 
    extends
        Service
{

    BoolMutable _draw = BoolMutable.broadcast(false);

    var arrowRight = false;
    var arrowLeft  = false;
    var arrowUp    = false;
    var arrowDown  = false;
    var space      = false;
    var enter      = false;

    void draw() {
        _draw.stream();
    }

    StreamSubscription<bool> onDraw(Function function) {
        return _draw.subscribe((event) {
            function();
        });
    }

}