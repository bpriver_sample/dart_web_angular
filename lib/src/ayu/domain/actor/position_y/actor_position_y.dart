
import 'package:sample_dart_web_angular/src/sample_dart_web_angular_module.dart';
import 'package:pub_base_model/between_x_and_y.dart';
import 'package:pub_base_model/model.dart';
import 'package:pub_base_model/num_state_model_action.dart';

class ActorPositionY
    extends
        NumStateModel
    with
        BetweenXAndY<
            num
            ,ActorPositionY
            ,SampleDartWebAngularModule
        >
        ,Between0And10000<
            num
            ,ActorPositionY
            ,SampleDartWebAngularModule
        >
        ,NumStateModelGetPx<
            num
        >
        ,BetweenXAndYOperator<
            num
            ,ActorPositionY
            ,SampleDartWebAngularModule
        >
{

    /// 0 から 10000 までの数（num 型）
    /// 単位は px.    
    ActorPositionY(this.value) {
        validationBetweenXAndY();
    }

    @override
    final num value;

    @override
    ActorPositionY internalFactory(num value) => ActorPositionY(value);

}