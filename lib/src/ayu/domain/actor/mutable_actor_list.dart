
import 'package:sample_dart_web_angular/src/sample_dart_web_angular_module.dart';
import 'package:pub_base_model/computation.dart';

import 'mutable_actor.dart';

class MutableActorList
    extends
        ListComputation<
            MutableActor
            ,MutableActorList
            ,SampleDartWebAngularModule
        >
{

    MutableActorList(this.values);

    @override
    final Iterable<MutableActor> values;

    @override
    MutableActorList internalFactory(Iterable<MutableActor> models) => MutableActorList(models);

}