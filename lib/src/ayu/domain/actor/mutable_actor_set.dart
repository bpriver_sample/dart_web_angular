
import 'package:sample_dart_web_angular/src/sample_dart_web_angular_module.dart';
import 'package:pub_base_model/computation.dart';
import 'package:pub_base_model/iterable_action.dart';
import 'package:pub_base_model/iterable_unique_object_model.dart';
import 'package:pub_base_model/set_action.dart';

import 'actor_barrel.dart';
import 'mutable_actor.dart';

class MutableActorSet
    extends
        SetComputation<
            MutableActor
            ,MutableActorSet
            ,SampleDartWebAngularModule
        >
    with
        IterableUniqueObjectModel<
            MutableActor
            ,MutableActorSet
            ,ActorIdentifier
            ,ActorIdentifierList
            ,SampleDartWebAngularModule
        >
        ,UniqueObjectModelSetGetById<
            MutableActor
            ,MutableActorSet
            ,ActorIdentifier
            ,ActorIdentifierList
            ,SampleDartWebAngularModule
        >
        ,IterableUniqueObjectModelRemoveById<
            MutableActor
            ,MutableActorSet
            ,ActorIdentifier
            ,ActorIdentifierList
            ,SampleDartWebAngularModule
        >
        ,UniqueObjectModelSetPut<
            MutableActor
            ,MutableActorSet
            ,ActorIdentifier
            ,ActorIdentifierList
            ,SampleDartWebAngularModule
        >
{

    MutableActorSet(this.values);

    @override
    final Iterable<MutableActor> values;

    @override
    MutableActorSet internalFactory(Iterable<MutableActor> models) => MutableActorSet(models);

    @override
    ActorIdentifierList get identifiers => ActorIdentifierList(values.map<ActorIdentifier>((e) => e.identifier));

}