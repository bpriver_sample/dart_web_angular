
import 'package:sample_dart_web_angular/src/sample_dart_web_angular_module.dart';
import 'package:pub_base_model/model.dart';
import 'package:pub_base_model/unique_object_model.dart';

import 'actor_barrel.dart';

export 'actor_list.dart';
export 'mutable_actor.dart';
export 'mutable_actor_list.dart';
export 'mutable_actor_set.dart';

class Actor
    extends
        ObjectModel<SampleDartWebAngularModule>
    with
        UniqueObjectModel<
            ActorIdentifier
            ,SampleDartWebAngularModule
        >
{

    Actor(this.identifier, this.positionX, this.positionY, this.positionZ, this.width, this.height, this.depth);
    @override
    final ActorIdentifier identifier;
    final ActorPositionX positionX;
    final ActorPositionY positionY;
    final ActorPositionZ positionZ;
    final ActorWidth width;
    final ActorHeight height;
    final ActorDepth depth;

    Actor changePositionX(ActorPositionX positionX) => Actor(identifier, positionX, positionY, positionZ, width, height, depth);
    Actor changePositionY(ActorPositionY positionY) => Actor(identifier, positionX, positionY, positionZ, width, height, depth);
    Actor changePositionZ(ActorPositionZ positionZ) => Actor(identifier, positionX, positionY, positionZ, width, height, depth);
    Actor changeWidth(ActorWidth width) => Actor(identifier, positionX, positionY, positionZ, width, height, depth);
    Actor changeHeight(ActorHeight height) => Actor(identifier, positionX, positionY, positionZ, width, height, depth);
    Actor changeDepth(ActorDepth depth) => Actor(identifier, positionX, positionY, positionZ, width, height, depth);

    Actor addPositionX(ActorPositionX positionX) => changePositionX(this.positionX + positionX);
    Actor addPositionY(ActorPositionY positionY) => changePositionY(this.positionY + positionY);
    Actor addPositionZ(ActorPositionZ positionZ) => changePositionZ(this.positionZ + positionZ);
    Actor addWidth(ActorWidth width) => changeWidth(this.width + width);
    Actor addHeight(ActorHeight height) => changeHeight(this.height + height);
    Actor addDepth(ActorDepth depth) => changeDepth(this.depth + depth);

    Actor subtractPositionX(ActorPositionX positionX) => changePositionX(this.positionX - positionX);
    Actor subtractPositionY(ActorPositionY positionY) => changePositionY(this.positionY - positionY);
    Actor subtractPositionZ(ActorPositionZ positionZ) => changePositionZ(this.positionZ - positionZ);
    Actor subtractWidth(ActorWidth width) => changeWidth(this.width - width);
    Actor subtractHeight(ActorHeight height) => changeHeight(this.height - height);
    Actor subtractDepth(ActorDepth depth) => changeDepth(this.depth - depth);

    @override
    Map<String, String> toPrimitive() {
        return {
            'identifier': identifier.toString(),
            'positionX': positionX.toString(),
            'positionY': positionY.toString(),
            'positionZ': positionZ.toString(),
            'width': width.toString(),
            'height': height.toString(),
            'depth': depth.toString(),
        };
    }

}