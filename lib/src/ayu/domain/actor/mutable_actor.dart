
import 'package:sample_dart_web_angular/src/sample_dart_web_angular_module.dart';
import 'package:pub_base_model/model.dart';
import 'package:pub_base_model/mutable.dart';
import 'package:pub_base_model/unique_object_model.dart';
import 'package:pub_debug_model/debug_model.dart';

import 'actor.dart';
import 'actor_barrel.dart';

class MutableActor
    extends
        NonNullableMutable<Actor>
    implements
        ObjectModel<SampleDartWebAngularModule>
        ,UniqueObjectModel<
            ActorIdentifier
            ,SampleDartWebAngularModule
        >
{

    MutableActor(Actor value) : super(value);
    MutableActor.broadcast(Actor value) : super.broadcast(value);

    void nextPositionX(ActorPositionX positionX) => next(value.changePositionX(positionX));
    void nextPositionY(ActorPositionY positionY) => next(value.changePositionY(positionY));
    void nextPositionZ(ActorPositionZ positionZ) => next(value.changePositionZ(positionZ));
    void nextWidth(ActorWidth width) => next(value.changeWidth(width));
    void nextHeight(ActorHeight height) => next(value.changeHeight(height));
    void nextDepth(ActorDepth depth) => next(value.changeDepth(depth));

    void addNextPositionX(ActorPositionX positionX) => next(value.addPositionX(positionX));
    void addNextPositionY(ActorPositionY positionY) => next(value.addPositionY(positionY));
    void addNextPositionZ(ActorPositionZ positionZ) => next(value.addPositionZ(positionZ));
    void addNextWidth(ActorWidth width) => next(value.addWidth(width));
    void addNextHeight(ActorHeight height) => next(value.addHeight(height));
    void addNextDepth(ActorDepth depth) => next(value.addDepth(depth));

    void subtractNextPositionX(ActorPositionX positionX) => next(value.subtractPositionX(positionX));
    void subtractNextPositionY(ActorPositionY positionY) => next(value.subtractPositionY(positionY));
    void subtractNextPositionZ(ActorPositionZ positionZ) => next(value.subtractPositionZ(positionZ));
    void subtractNextWidth(ActorWidth width) => next(value.subtractWidth(width));
    void subtractNextHeight(ActorHeight height) => next(value.subtractHeight(height));
    void subtractNextDepth(ActorDepth depth) => next(value.subtractDepth(depth));

    @override
    String operator [](String key) {
        if(toPrimitive().containsKey(key)) return toPrimitive()[key] as String;
        throw ProductError(
            SampleDartWebAngularModule,
            runtimeType,
            [
                'error の発生場所 => $runtimeType.operator []',
                '入力した key => $key ',
                '有効な key の一覧 => ${toPrimitive().keys}',
            ]
        );
    }

    @override
    ActorIdentifier get identifier => value.identifier;

    @override
    Map<String, String> toPrimitive() => value.toPrimitive();

}