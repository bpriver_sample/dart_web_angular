
import 'package:sample_dart_web_angular/src/sample_dart_web_angular_module.dart';
import 'package:pub_base_model/between_x_and_y.dart';
import 'package:pub_base_model/model.dart';
import 'package:pub_base_model/num_state_model_action.dart';

class ActorPositionZ
    extends
        NumStateModel
    with
        BetweenXAndY<
            num
            ,ActorPositionZ
            ,SampleDartWebAngularModule
        >
        ,Between0And10000<
            num
            ,ActorPositionZ
            ,SampleDartWebAngularModule
        >
        ,NumStateModelGetPx<
            num
        >
        ,BetweenXAndYOperator<
            num
            ,ActorPositionZ
            ,SampleDartWebAngularModule
        >
{

    /// 0 から 10000 までの数（num 型）
    /// 単位は px.    
    ActorPositionZ(this.value) {
        validationBetweenXAndY();
    }

    @override
    final num value;

    @override
    ActorPositionZ internalFactory(num value) => ActorPositionZ(value);

}