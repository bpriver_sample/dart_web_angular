
import 'package:sample_dart_web_angular/src/ayu/configuration/configuration_barrel.dart';
import 'package:sample_dart_web_angular/src/ayu/domain/domain_barrel.dart';
import 'package:sample_dart_web_angular/src/ayu/status/window/window_status_barrel.dart';

class Store {

    late MutableActorSet actorSet;
    late MutableWindowConfigurationSet windowConfigurationSet;

    ActorIdentifier createActorWithWindow(WindowStatusIdentifier linkWindowIdentifier, {bool broadcast = false}) {
        final linkWindow = windowConfigurationSet.getById(linkWindowIdentifier).value;
        final result = Actor(
            ActorIdentifier(),
            ActorPositionX(linkWindow.windowStatus.x.value),
            ActorPositionY(linkWindow.windowStatus.y.value),
            ActorPositionZ(0),
            ActorWidth(linkWindow.windowStatus.width.value),
            ActorHeight(linkWindow.windowStatus.height.value),
            ActorDepth(0),
        );
        if (broadcast) {
            actorSet = actorSet.put(MutableActor.broadcast(result));
        } else {
            actorSet = actorSet.put(MutableActor(result));
        }
        return result.identifier;
    }

    WindowStatusIdentifier createWindowConfiguration(WindowConfiguration windowConfiguration,) {
        windowConfigurationSet = windowConfigurationSet.put(MutableWindowConfiguration(windowConfiguration));
        return windowConfiguration.windowStatus.identifier;
    }
    
}