
import 'dart:html';

import 'package:angular/angular.dart';

import 'package:sample_dart_web_angular/src/presentation/presentation_layer.dart';
import 'presentation_change_status.dart';

import 'presentation_change_directive.template.dart' as temp;

export 'presentation_change_status.dart';

/// 
/// 検証結果
/// 
@Component(
    selector: 'sample-presentation-change',
    templateUrl: 'presentation_change_directive.html',
    directives: [
        FinalInputStatusDirective,
    ],
    exports: [
    ],
)
class PresentationChangeDirective
    implements
        OnInit
        ,AfterChanges
{

    static final template = temp.PresentationChangeDirectiveNgFactory;

    PresentationChangeDirective(this.el);
    final HtmlElement el;

    // FinalInputStatus status = FinalInputStatus('@Input');

    @Input()
    PresentationChangeStatus status = PresentationChangeStatus(
        FinalInputStatus('valueA'),
        FinalInputStatus('valueB'),
        FinalInputStatus('valueC'),
    );

    @HostListener('click')
    void change() {
        status = PresentationChangeStatus(
            FinalInputStatus('valueAA'),
            FinalInputStatus('valueBB'),
            FinalInputStatus('valueCC'),
        );
        print('$runtimeType : excute change()');
    }

    @override
    void ngOnInit() {
        el.style.display = 'block';
        // el.style.backgroundColor = 'blue';
        el.style.backgroundColor = 'Lightgreen';
        print(status);
    }

    @override
    void ngAfterChanges() {
    }

}