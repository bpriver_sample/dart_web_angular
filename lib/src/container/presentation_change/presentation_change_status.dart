
import 'package:sample_dart_web_angular/src/presentation/presentation_layer.dart';

class PresentationChangeStatus {
    
    PresentationChangeStatus(this.valueA, this.valueB, this.valueC);
    final FinalInputStatus valueA;
    final FinalInputStatus valueB;
    final FinalInputStatus valueC;

}